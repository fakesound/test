<?php

namespace App\Payments\DataObjects;

class PaymentData
{
    public string $id;

    public string $amount;

    public string $redirectUrl;
}
