<?php

namespace App\Payments;

use App\Payments\DataObjects\PaymentData;
use App\Payments\DataObjects\ResultData;

interface PaymentSystem
{
    public function createTransaction(PaymentData $data): ResultData;

    public function isTransactionSuccessfull(PaymentData $data): bool;
}
