<?php

namespace App\Payments;

use App\Models\Payment;

interface PaymentSystemFactory
{
    public function make(Payment $payment): PaymentSystem;
}
