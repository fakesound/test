<?php

namespace App\Payments;

interface PaymentSystemFactoryResolver
{
    public function resolve(string $paymentSystem): PaymentSystemFactory;
}
