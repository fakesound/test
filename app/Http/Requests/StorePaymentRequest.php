<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePaymentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'amount' => [
                'required',
            ],
            'order_number' => [
                'required',
            ],
            'return_url' => [
                'required',
            ],
            'api_key' => [
                'required',
                Rule::exists('users', 'api_key'),
            ],
        ];
    }
}
