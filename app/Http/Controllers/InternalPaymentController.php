<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentRequest;
use App\Http\Resources\PaymentResource;
use App\Models\Payment;
use App\Payments\DataObjects\PaymentData;
use App\Payments\PaymentSystemFactoryResolver;

class InternalPaymentController extends Controller
{
    public function store(StorePaymentRequest $request)
    {
        // Создаем платеж в нашей системе
        // Отвечаем ссылкой на форму по этому платежу

        $payment = Payment::create(
            $request->validated()
        );

        return [
            'form_url' => route('form', ['payment' => $payment->id]),
        ];
    }

    public function show(Payment $payment)
    {
        // Показываем форму для оплаты

        return PaymentResource::make($payment);
    }

    public function result(Payment $payment, PaymentSystemFactoryResolver $resolver)
    {
        // Показываем страницу статуса оплаты пользователю

        $factory = $resolver->resolve(
            $payment->system
        );

        $system = $factory->make($payment);

        $result = $system->isTransactionSuccessfull(
            PaymentData::fromModel($payment)
        );

        $payment->update([
            'status' => $result ? 'success' : 'fail',
        ]);

        return PaymentResource::make($payment);
    }

    public function paymentStatus(Request $request, Payment $payment)
    {
        // По номеру заказа и API ключу отдаем статус платежа для сторонних сервисов

        return [
            'status' => $payment->status,
        ];
    }
}
