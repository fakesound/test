<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Payments\DataObjects\PaymentData;
use App\Payments\PaymentSystemFactoryResolver;
use Illuminate\Http\Request;

class ExternalPaymentController extends Controller
{
    public function proceedToPayment(
        Request $request,
        Payment $payment,
        PaymentSystemFactoryResolver $resolver,
    ) {
        // Получаем платеж и какой сервис использовать
        // Создаем транзакцию в платежной системе
        // Получаем ссылку для перехода
        // Перенаправляем пользователя

        $factory = $resolver->resolve(
            $request->validated('payment_system')
        );

        $system = $factory->make($payment);

        $result = $system->createTransaction(
            PaymentData::fromModel($payment)
        );

        return response()->away($result->url);
    }
}
