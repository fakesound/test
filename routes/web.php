<?php

use App\Http\Controllers\ExternalPaymentController;
use App\Http\Controllers\InternalPaymentController;
use Illuminate\Support\Facades\Route;

Route::post('payment', [InternalPaymentController::class, 'store']);
Route::get('payment/{payment}', [InternalPaymentController::class, 'show'])->name('form');
Route::get('payment/{payment}/result', [InternalPaymentController::class, 'result'])->name('result');
Route::get('payment/{payment}/status', [InternalPaymentController::class, 'paymentStatus']);

Route::post('payment/{payment}/proceed', [ExternalPaymentController::class, 'proceedToPayment']);
